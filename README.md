## Summary

"Just enough" React

## Local development

### Linux / macOS

#### Preliminary steps

- Install Docker and ensure the Docker daemon is running
- Clone the repository `git clone https://gitlab.com/kinggu/react-docs.git`
- `cd /path/to/react-docs`
- Ensure you have the correct Git user for the repository:
  - `git config user.name`
  - `git config user.email`
  - If name needs updating, `git config user.name <new-name>`
  - If email needs updating, `git config user.email <new-email>`

#### Run website

- `docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material`
- Navigate to `http://localhost:8000/` in a browser, you should see the docs

#### Viewing on mobile / tablet

If you have devices connected to the same local network as your development
machine, e.g. same WiFi router, you can view `localhost` on those devices:

- Display private / local IP addresses on your development machine
  `ifconfig | grep "inet " | grep -v 127.0.0.1`
- On the device, try each IP address at port 3000 / 9009 until you see the site
  / stories

## Deployment

Pushes to master should deploy automatically to
`https://kinggu.gitlab.io/react-docs/`
