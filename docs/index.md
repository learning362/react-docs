## HTML

A web browser can load and render an HTML file, e.g.

`test.html`

```html
<!DOCTYPE html>
<html>
  <title>My title</title>
  <head>
    <!-- head definitions go here -->
  </head>
  <body>
    Hello, world!
  </body>
</html>
```

You can view the page by entering the path of the HTML file into your browser,
e.g. `file:///home/guy/Documents/react-docs/test.html`

### DOM (Document Object Model)

When a web page is loaded, the browser creates a Document Object Model of the
page.

The HTML DOM model is constructed as a tree of Objects. In the above example,
the document consists of a tree of objects. The root element of the tree is
`<html>` as per the opening and closing `<html>` tags.

In our root element, there are three child elements `<title>`, `<head>` and
`<body>`.

### JavaScript

#### Adding JavaScript to HTML

In our HTML file, we can add some JavaScript inline

```html
<!DOCTYPE html>
<html>
  <title>My title</title>
  <head>
    <!-- head definitions go here -->
  </head>
  <body>
    Hello, world!
    <script>
      console.log("Hello from JavaScript");
    </script>
  </body>
</html>
```

or via a file, in the same directory as `path/to/test.html`, have another file
`/path/to/test.js`

`test.html`

```html
<!DOCTYPE html>
<html>
  <title>My title</title>
  <head>
    <!-- head definitions go here -->
  </head>
  <body>
    Hello, world!
    <script src="test.js"></script>
  </body>
</html>
```

`test.js`

```javascript
console.log("Hello from JavaScript");
```

You will see the same web page in the browser and when you inspect the console
you will see the `Hello from JavaScript`.

#### DOM API

In the above example, all the JavaScript did was log a string in the browser
console.

JavaScript can do more than that, it can interact with the DOM via the DOM API.

`test.js`

```javascript
const body = document.getElementsByTagName("body")[0];
const h1 = document.createElement("h1");
h1.appendChild(document.createTextNode("Hello from JavaScript"));
body.appendChild(h1);
```

The string in the console is now viewable on the web page inside an `h1`
element.

We could have done the same thing in HTML

`test.html`

```html
<!DOCTYPE html>
<html>
  <title>My title</title>
  <head>
    <!-- head definitions go here -->
  </head>
  <body>
    Hello, world!
    <h1>Hello from JavaScript</h1>
  </body>
</html>
```

#### React

We can do the same thing as above, only know we use React, a JavaScript library,
which basically gives us an API to do the same thing above, i.e. it wraps around
the DOM API.

We add some libraries (actually two because React works for mobile as well so as
we in web we need the specific library for DOM)

`index.html`

```html
<!DOCTYPE html>
<html>
  <title>My title</title>
  <head>
    <!-- head definitions go here -->
  </head>
  <body>
    Hello, world!
    <script
      crossorigin
      src="https://unpkg.com/react@16/umd/react.development.js"
    ></script>
    <script
      crossorigin
      src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"
    ></script>
    <script src="test.js"></script>
  </body>
</html>
```

`test.js`

```javascript
const body = document.getElementsByTagName("body")[0];
const text = React.createElement("div", null, "Hello, world!");
const title = React.createElement("h1", null, "Hello from JavaScript");
const container = React.createElement("div", null, [text, title]);
ReactDOM.render(container, body);
```

Because React wipes out anything else that is inside the element being mounted,
we have to write out the hello world bit ourselves.

We can call render again, this overwrites what was there previously

`test.js`

```javascript
const body = document.getElementsByTagName("body")[0];
const text = React.createElement("div", null, "Hello, world!");
const title = React.createElement("h1", null, "Hello from JavaScript");
const container = React.createElement("div", null, [text, title]);
ReactDOM.render(container, body);
ReactDOM.render(text, body);
```

above means only see hello world.

Can also render in multiple places in the DOM

`test.html`

```html
<!DOCTYPE html>
<html>
  <title>My title</title>
  <head>
    <!-- head definitions go here -->
  </head>
  <body>
    <div id="div1"></div>
    <div id="div2"></div>
    <script
      crossorigin
      src="https://unpkg.com/react@16/umd/react.development.js"
    ></script>
    <script
      crossorigin
      src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"
    ></script>
    <script src="test.js"></script>
  </body>
</html>
```

`test.js`

```javascript
const div1 = document.getElementById("div1");
const div2 = document.getElementById("div2");
const title = React.createElement("h1", null, "Hello from JavaScript");
const text = React.createElement("div", null, "Hello, universe!");
ReactDOM.render(title, div1);
ReactDOM.render(text, div2);
```

You see each one hello from javascript and hello universe separately in the page
